package com.company.WatchService;

import java.io.IOException;
import java.net.Socket;
import java.nio.channels.SocketChannel;
import java.nio.file.*;

public class Main {

    public static void main(String[] args) throws IOException, InterruptedException {
	// write your code here
       WatchService service = FileSystems.getDefault().newWatchService();

        Path path = Paths.get("/home/xw/IdeaProjects/WatchService/");
        WatchKey watchKey = path.register(service, StandardWatchEventKinds.ENTRY_MODIFY,
                StandardWatchEventKinds.ENTRY_CREATE,
                StandardWatchEventKinds.OVERFLOW);

        //watchKey = service.poll();
        while ((watchKey = service.take()) != null){
            for (WatchEvent<?> event: watchKey.pollEvents()){
                System.out.println("Event " + event.kind() + " File affected on " +
                event.context());
            }
            watchKey.reset();
        }

    }
}
